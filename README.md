# 文章配套代码

哈喽，我是子牙，一个很卷的硬核男人

深入研究计算机底层、Windows内核、Linux内核、Hotspot源码……聚焦做那些大家想学没地方学的课程。为了保证课程质量及教学效果，一年磨一剑，三年先后做了这些课程：手写JVM、手写OS、带你用纯汇编写OS、手写64位多核OS、实战Linux内核…

### 文章《Linux内核中，CPU是如何读写数据的》

二维码

![](erweima.png)

### 公众号[硬核子牙]

聚集分享linux内核、windows内核、java虚拟机、逆向方向的文章

![](yingheziya.png)

# 如何体验

### 安装驱动
> 1、将main.c运行起来
> 
> 2、在命令行终端输入dmesg -w，查看内核输出
> 
> 2、sudo insmod ziya_btech.ko pid=7031 vaddr=0x400624
> 
> 这里的pid、vaddr换成你运行main.c显示的，即可看到效果